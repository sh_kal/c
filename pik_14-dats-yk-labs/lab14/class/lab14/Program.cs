﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab14
{
    class Program
    {
        static void workWithLine()
        {
            int x1, y1, x2, y2;
            Console.WriteLine("Введите координаты начала линии");
            x1 = int.Parse(Console.ReadLine());
            y1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите координаты конца линии");
            x2 = int.Parse(Console.ReadLine());
            y2 = int.Parse(Console.ReadLine());
            Line line = new Line(x1, y1, x2, y2);
            Console.WriteLine("Длина линии {0}", Line.getLine(line));
        }
        static void workWithRectanhle()
        {
            int x1, y1, x2, y2, x3, y3, x4, y4;
            Console.WriteLine("Введите координаты для стороны А:");
            x1 = int.Parse(Console.ReadLine());
            y1 = int.Parse(Console.ReadLine());
            x2 = int.Parse(Console.ReadLine());
            y2 = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите координаты для стороны В");
            x3 = int.Parse(Console.ReadLine());
            y3 = int.Parse(Console.ReadLine());
            x4 = int.Parse(Console.ReadLine());
            y4 = int.Parse(Console.ReadLine());
            Rectangle rect = new Rectangle(x1, y1, x2, y2, x3, y3, x4, y4);
            Console.WriteLine("Периметр прямоугольника: {0}\nПлощадь прямоугольника: {1}", Rectangle.perimetr(rect), Rectangle.getArea(rect));
        }
        static void workWithCircle()
        {
            int x1, y1, x2, y2;
            Console.WriteLine("Введите координаты начала радиуса");
            x1 = int.Parse(Console.ReadLine());
            y1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите координаты конца радиуса");
            x2 = int.Parse(Console.ReadLine());
            y2 = int.Parse(Console.ReadLine());
            Circle circ = new Circle(x1, y1, x2, y2);
            Console.WriteLine("Диаметр {0}\nПлощадь круга {1}\nДлина круга {2}", Circle.dia(circ),Circle.getArea(circ),Circle.length(circ));
        }
        static void workWithEllipse()
        {
            int x1, y1, x2, y2, x3, y3, x4, y4;
            Console.WriteLine("Введите координаты для полуоси А:");
            x1 = int.Parse(Console.ReadLine());
            y1 = int.Parse(Console.ReadLine());
            x2 = int.Parse(Console.ReadLine());
            y2 = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите координаты для полуоси В");
            x3 = int.Parse(Console.ReadLine());
            y3 = int.Parse(Console.ReadLine());
            x4 = int.Parse(Console.ReadLine());
            y4 = int.Parse(Console.ReadLine());
            Ellipse el = new Ellipse(x1, y1, x2, y2, x3, y3, x4, y4);
            Console.WriteLine("Длина эллипса: {0}\nПлощадь эллипса: {1}", Ellipse.gArea(el), Ellipse.gL(el));
        }
        static void menu()
        {
            bool t = true;
            while (t)
            {
                int q;
                Console.WriteLine("1. Класс Line\n2. Класс Rectangle\n3. Класс Circle\n4. Класс Ellipse\n5. Выход");
                q = int.Parse(Console.ReadLine());
                switch (q)
                {
                    case 1:
                        Console.Clear();
                        workWithLine();
                        break;
                    case 2:
                        Console.Clear();
                        workWithRectanhle();
                        break;
                    case 3:
                        Console.Clear();
                        workWithCircle();
                        break;
                    case 4:
                        Console.Clear();
                        workWithEllipse();
                        break;
                    case 5:
                        t = false;
                        break;
                    default:
                        Console.WriteLine();
                        break;
                }
            }
        }
        static void Main(string[] args)
        {
            menu();
        }
    }
}
