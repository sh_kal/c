﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab14
{
    class Circle:Point
    {
        private Point point1, point2;
        public Circle(int X,int Y,int x1,int y1) : base(X, Y)
        {
            point1 = new Point(X, Y);
            point2 = new lab14.Point(x1, y1);
        }
        public static double dia(Circle c)
        {
            return Point.getLength(c.point1, c.point2) + Point.getLength(c.point1, c.point2);
        }
        public static double getArea(Circle c)
        {
            return 3.14 * (Point.getLength(c.point1, c.point2) * Point.getLength(c.point1, c.point2));
        }
        public static double length(Circle c)
        {
            return 2 * 3.14 * Point.getLength(c.point1, c.point2);
        }
    }
}
