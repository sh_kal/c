﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab14
{
    class Rectangle:Line
    {
        private Line line1, line2;
        public Rectangle(int X,int Y,int x1,int y1,int X1,int Y1,int x2,int y2):base(X,Y,x1,y1)
        {
            line1 = new Line(X, Y, x1, y1);
            line2 = new Line(X1, Y1, x2, y2);
        }
        public static double perimetr(Rectangle r)
        {
            return (Line.getLine(r.line1) + Line.getLine(r.line2)) * 2;
        }
        public static double getArea(Rectangle r)
        {
            return Line.getLine(r.line1) * Line.getLine(r.line2);
        }
    }
}
