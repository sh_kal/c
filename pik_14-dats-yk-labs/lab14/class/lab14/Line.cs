﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab14
{
    class Line:Point
    {
        protected Point point1, point2;
        public Line()
        {
            point1 = new Point();
            point2 = new Point(3, 2);
        }
        public Line(int X,int Y,int x1,int y1):base(X,Y)
        {
            point1 = new Point(X, Y);
            point2 = new Point(x1, y1);
        }
        public static double getLine(Line l)
        {
            return Point.getLength(l.point1, l.point2);
        }
    }
}
