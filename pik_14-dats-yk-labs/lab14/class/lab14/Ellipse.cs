﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab14
{
    class Ellipse:Circle
    {
        private Point point1, point2, point3, point4;
        public Ellipse(int X,int Y,int x1,int y1,int X1,int Y1,int x2,int y2) : base(X, Y, x1, y1)
        {
            point1 = new Point(X, Y);
            point2 = new Point(x1, y1);
            point3 = new Point(X1, Y1);
            point4 = new Point(x2, y2);
        }
        public static double gArea(Ellipse e)
        {
            return 3.14 * Point.getLength(e.point1, e.point2) * Point.getLength(e.point3, e.point4);
        }
        public static double gL(Ellipse e)
        {
            double a = Point.getLength(e.point1, e.point2);
            double b = Point.getLength(e.point3, e.point4);
            return 4 * ((3.14 * a * b + (a - b)) / (a + b));
        }
    }
}
