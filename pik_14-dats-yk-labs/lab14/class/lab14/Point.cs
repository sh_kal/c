﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab14
{
    class Point
    {
        protected int x, y;
        public Point()
        {
            x = 0;
            y = 0;
        }
        public Point(int X,int Y)
        {
            x = X;
            y = Y;
        }
        protected static double getLength(Point p1,Point p2)
        {
            return Math.Sqrt(Math.Pow(p2.x - p1.x, 2) + Math.Pow(p2.y - p1.y, 2));
        }
    }
}
