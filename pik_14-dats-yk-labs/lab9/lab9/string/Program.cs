﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace @string
{
    class Program
    {
        static void word(string s)
        {
            List<string> w = s.Split(new[] { ' ' }).ToList();
            w.Sort();
            Console.WriteLine(string.Join("\n", w));
        }
        static void popular(string s)
        {
            char q = ' ';
            int l, z = 0, i, j;
            for (i = 0; i < s.Length; i++)
            {
                l = 0;
                for (j = 0; j < s.Length; j++)
                {
                    if (s[i] == s[j])
                        l++;
                }
                if (l > z)
                {
                    z = l;
                    q = s[i];
                }
            }
            Console.WriteLine("Popular -  {0}",q);
        }
        static void Main(string[] args)
        {
            string s = Console.ReadLine();
            word(s);
            popular(s);
        }
    }
}
