﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Wpf4
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            label.Content = "Введите y:";
            label_Copy.Content = "Введите z:";
            label_Copy1.Content = "f:";
            button.Content = "Посчитать";
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            double y, z, f = 0;
            y = double.Parse(textBox.Text);
            z = double.Parse(textBox_Copy.Text);
            f = Math.Pow(y + Math.Pow(4 - 1, 1 / 3.0), 1 / 4.0) / (Math.Abs(4 - y) * Math.Pow(Math.Sin(z), 2) + Math.Tan(z));
            label_Copy1.Visibility = Visibility.Visible;
            textBox_Copy1.Visibility = Visibility.Visible;
            textBox_Copy1.Text = f.ToString();
        }
    }
}
