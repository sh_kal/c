﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Console5
{
    class Program
    {
        static void Main(string[] args)
        {
            double y, z, t = 0;
            Console.Write("Введите y: ");
            y = double.Parse(Console.ReadLine());
            Console.Write("Введите z: ");
            z = double.Parse(Console.ReadLine());
            t = (2 * Math.Cos(4 - 3.14 / 6)) / (0.5 + Math.Pow(Math.Sin(y), 2)) * (1 + (z * z) / (3 - (z * z) / 5));
            Console.WriteLine("t = {0}", t);   
        }
    }
}
