﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Form2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            label1.Text = "Введите a:";
            label2.Text = "Введите b:";
            label3.Text = "P:";
            label4.Text = "S:";
            button1.Text = "Посчитать";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int a, b, P, S;
            a = int.Parse(textBox1.Text);
            b = int.Parse(textBox2.Text);
            P = a * 2 + b * 2;
            S = a * b;
            label3.Visible = true;
            label4.Visible = true;
            textBox3.Visible = true;
            textBox4.Visible = true;
            textBox3.Text = P.ToString();
            textBox4.Text = S.ToString();
        }
    }
}
