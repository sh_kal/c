﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApplication2
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            label.Content = "Введите a:";
            label_Copy.Content = "Введите b:";
            label_Copy1.Content = "f:";
            button.Content = "Посчитать";
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            double a, b, f = 0;
            a = double.Parse(textBox.Text);
            b = double.Parse(textBox_Copy.Text);
            f = 2.4 * Math.Abs((16 + b) / a) + (a - b) * Math.Pow(Math.Sin(a - b), 2) + Math.Pow(10, -2) * (4 - b);
            label_Copy1.Visibility = Visibility.Visible;
            textBox_Copy1.Visibility = Visibility.Visible;
            textBox_Copy1.Text = f.ToString();
        }
    }
}
