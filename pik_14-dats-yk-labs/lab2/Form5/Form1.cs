﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Form5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            label1.Text = "Введите y:";
            label2.Text = "Введите z:";
            label3.Text = "t:";
            button1.Text = "Посчитать";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double z, y, t = 0;
            y = double.Parse(textBox1.Text);
            z = double.Parse(textBox2.Text);
            t = (2 * Math.Cos(4 - 3.14 / 6)) / (0.5 + Math.Pow(Math.Sin(y), 2)) * (1 + (z * z) / (3 - (z * z) / 5));
            label3.Visible = true;
            textBox3.Visible = true;
            textBox3.Text = t.ToString();
        }
    }
}
