﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Console4
{
    class Program
    {
        static void Main(string[] args)
        {
            double y, z, f = 0;
            Console.Write("Введите y: ");
            y = double.Parse(Console.ReadLine());
            Console.Write("Введите z: ");
            z = double.Parse(Console.ReadLine());
            f = Math.Pow(y + Math.Pow(4 - 1, 1 / 3.0), 1 / 4.0) / (Math.Abs(4 - y) * Math.Pow(Math.Sin(z), 2)+Math.Tan(z));
            Console.WriteLine("f = {0}", f);
        }
    }
}
