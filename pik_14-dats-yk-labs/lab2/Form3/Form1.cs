﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Form3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            label1.Text = "Введите a:";
            label2.Text = "Введите b:";
            label3.Text = "f:";
            button1.Text = "Посчитать";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double a, b, f = 0;
            a = double.Parse(textBox1.Text);
            b = double.Parse(textBox2.Text);
            f = 2.4 * Math.Abs((16 + b) / a) + (a - b) * Math.Pow(Math.Sin(a - b), 2) + Math.Pow(10, -2) * (4 - b);
            label3.Visible = true;
            textBox3.Visible = true;
            textBox3.Text = f.ToString();
        }
    }
}
