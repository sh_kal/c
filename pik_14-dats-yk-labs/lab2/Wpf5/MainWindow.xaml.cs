﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Wpf5
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            label.Content = "Введите y:";
            label_Copy.Content = "Введите z:";
            label_Copy1.Content = "t:";
            button.Content = "Посчитать";
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            double y, z, t = 0;
            y = double.Parse(textBox.Text);
            z = double.Parse(textBox_Copy.Text);
            t = (2 * Math.Cos(4 - 3.14 / 6)) / (0.5 + Math.Pow(Math.Sin(y), 2)) * (1 + (z * z) / (3 - (z * z) / 5));
            label_Copy1.Visibility = Visibility.Visible;
            textBox_Copy1.Visibility = Visibility.Visible;
            textBox_Copy1.Text = t.ToString();
        }
    }
}
