﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Console2
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b, P, S;
            Console.Write("Введите сторону a: ");
            a = int.Parse(Console.ReadLine());
            Console.Write("Введите сторону b: ");
            b = int.Parse(Console.ReadLine());
            P = a * 2 + b * 2;
            S = a * b;
            Console.WriteLine("Периметр = {0} Площадь = {1}", P, S);
        }
    }
}
