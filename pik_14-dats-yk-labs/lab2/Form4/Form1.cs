﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Form4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            label1.Text = "Введите y:";
            label2.Text = "Введите z:";
            label3.Text = "f:";
            button1.Text = "Посчитать";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double z, y, f = 0;
            y = double.Parse(textBox1.Text);
            z = double.Parse(textBox2.Text);
            label3.Visible = true;
            f = Math.Pow(y + Math.Pow(4 - 1, 1 / 3.0), 1 / 4.0) / (Math.Abs(4 - y) * Math.Pow(Math.Sin(z), 2) + Math.Tan(z));
            textBox3.Visible = true;
            textBox3.Text = f.ToString();
        }
    }
}
