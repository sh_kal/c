﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Wpf2
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            label.Content = "Введите a:";
            label_Copy.Content = "Введите b:";
            label_Copy1.Content = "P:";
            label_Copy2.Content = "S:";
            button.Content = "Посчитать";
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            int a, b, P, S;
            a = int.Parse(textBox.Text);
            b = int.Parse(textBox_Copy.Text);
            P = a * 2 + b * 2;
            S = a * b;
            label_Copy1.Visibility = Visibility.Visible;
            label_Copy2.Visibility = Visibility.Visible;
            textBox_Copy1.Visibility = Visibility.Visible;
            textBox_Copy2.Visibility = Visibility.Visible;
            textBox_Copy1.Text = P.ToString();
            textBox_Copy2.Text = S.ToString();
        }
    }
}
