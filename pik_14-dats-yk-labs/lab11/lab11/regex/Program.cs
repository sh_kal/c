﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;

namespace regex
{
    class Program
    {
        static void regex(string s,string pattern)
        {
            Console.Clear();
            Regex reg = new Regex(pattern);
            foreach (Match m in reg.Matches(s))
            {
                Console.WriteLine(m.Groups[1].Value);
            }
        }
        static void regex2(string s, string pattern)
        {
            string url = "https://www.gismeteo.ua/ua/weather-zhytomyr-4943/";
            string html = string.Empty;
            HttpWebRequest myHttpWebRequest = (HttpWebRequest)HttpWebRequest.Create(url);
            HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
            StreamReader myStreamReader = new StreamReader(myHttpWebResponse.GetResponseStream());
            html = myStreamReader.ReadToEnd();


            Regex regex = new Regex(pattern);

            Console.WriteLine(s);
            foreach (Match match in regex.Matches(html))
            {
                Console.WriteLine(match.Groups[1].Value);
            }
        }
        static void zavd1()
        {
            string s, pattern;
            bool t = true;
            while (t)
            {
                int q;
                Console.WriteLine("1. Завдання 1.1\n2. Завдання 1.2\n3. Завдання 1.3\n4. Завдання 1.4\n5. Завдання 1.5 \n6. Завдання 1.6\n7. Повернутись до попереднього меню");
                q = int.Parse(Console.ReadLine());
                switch (q)
                {
                    case 1:
                        s = "hello <table> word</table> in the <td> whole word</td>" + "<table> qqqqq</table>";
                        pattern = @"<table>([a-z 0-9]+\s?)<\/table>";
                        regex(s, pattern);
                        break;
                    case 2:
                        s = "hello <table> word</table> in the <td> whole word</td>" + "<table> qqqqq</table>";
                        pattern = @"<td>([a-z 0-9]+\s?)<\/td>";
                        regex(s, pattern);
                        break;
                    case 3:
                        s = "hello <table> word</table> in the <td> whole word</td>" + "<table> qqqqq</table>"+"1210123434";
                        pattern = @"([0-9]+\.?([0-9]+)?)";
                        regex(s, pattern);
                        break;
                    case 4:
                        s = "hello <table> word</table> in the <td> whole word</td>" + "<table> qqqqq</table>"+ "foo@demo.net	bar.ba@test.co.uk   ";
                        pattern = @"([a-z 0-9]+((\.)?([a-z 0-9]+)?)?\@[a-z]+\.[a-z]+)";
                        regex(s, pattern);
                        break;
                    case 5:
                        s = "hello word <img src='image.jpeg'>";
                        pattern = @"(<img src='.+'>)";
                        regex(s, pattern);
                        break;
                    case 6:
                        Console.Clear();
                        s = "https://mediatemple.net http http://regexr.com";
                        pattern = @"(http(s)?:\/\/[a-z 0-9]+\.[a-z]+)";
                        string a = "<a href='$1'>Link</a>";
                        s = Regex.Replace(s, pattern, a);
                        Console.WriteLine(s);
                        break;
                    case 7:
                        t = false;
                        break;
                    default:
                        break;
                }
            }
        }
        static void zavd2()
        {
            Console.Clear();
            string s = "Температура на даний момент:";
            string pattern = "<dd class='value m_temp c'>(.+)<span class=\"meas\">&deg;C</span></dd>";
            regex2(s, pattern);
            s = "Швидкість вітру:";
            pattern = "<dd class='value m_wind ms' style='display:inline'>(.+)<span class=\"unit\">м/с</span></dd>";
            regex2(s, pattern);
            s = "Тиск:";
            pattern = "<dd class='value m_press torr'>(.+)<span class=\"unit\">мм рт. ст.</span></dd>";
            regex2(s, pattern);
        }
        static void zavd3()
        {
            string s = "\t C00l_Pass" +
                            "\t SupperPas1" +
                            "\t Cool_pass" +
                            "\t CSDSD123" +
                            "\t 00ldA";
            string pattern = @"(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9_]{8,}";
            Regex regex = new Regex(pattern);
            foreach (Match match in regex.Matches(s))
            {
                Console.WriteLine(match.Value);
            }

        }
        static void menu()
        {
            bool t = true;
            while (t)
            {
                int q;
                Console.WriteLine("1. Завдання 1\n2. Завдання 2\n3. Завдання 3");
                q = int.Parse(Console.ReadLine());
                switch (q)
                {
                    case 1:
                        zavd1();
                        break;
                    case 2:
                        zavd2();
                        break;
                    case 3:
                        Console.Clear();
                        Console.WriteLine("Валідні паролі");
                        zavd3();
                        break;
                    case 4:
                        t = false;
                        break;
                    default:
                        Console.WriteLine();
                        break;

                }
            }
        }
        static void Main(string[] args)
        {
            menu();
        }
    }
}
