﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b, c, d;
            double x1, x2;
            Console.Write("Введите a ");
            a = int.Parse(Console.ReadLine());
            Console.Write("Введите b ");
            b = int.Parse(Console.ReadLine());
            Console.Write("Введите с ");
            c = int.Parse(Console.ReadLine());
            d = b * b - 4 * a * c;
            if (d > 0)
            {
                x1 = (-b + Math.Sqrt(d)) / (2 * a);
                x2 = (-b - Math.Sqrt(d)) / (2 * a);
                Console.WriteLine("D= "+d+"\nx1= "+x1+"\nx2= "+x2);
            }
            else if (d == 0) {
                x1 = -b / (2 * a);
                Console.WriteLine("x=" + x1);
            }
            else
            {
                Console.WriteLine("Уравнение корней не имеет");
            }
        }
    }
}
