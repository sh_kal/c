﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace lab3_WPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            int a, b, c, d;
            double x1, x2;
            a = int.Parse(a1.Text);
            b = int.Parse(b1.Text);
            c = int.Parse(c1.Text);
            d = b * b - 4 * a * c;
            d_.Visibility = Visibility.Visible;
            d_.Content = "D = ";
            D.Visibility = Visibility.Visible;
            D.Text = d.ToString();
            if (d > 0)
            {
                x1 = (-b + Math.Sqrt(d)) / (2 * a);
                x2 = (-b - Math.Sqrt(d)) / (2 * a);
                x1_.Visibility = Visibility.Visible;
                x1_.Content = "x1=";
                X1.Visibility = Visibility.Visible;
                X1.Text = x1.ToString();
                x2_.Visibility = Visibility.Visible;
                x2_.Content = "x2=";
                X2.Visibility = Visibility.Visible;
                X2.Text = x2.ToString();
            }
            else if (d == 0)
            {
                x1 = -b / (2 * a);
                x1_.Visibility = Visibility.Visible;
                x1_.Content = "x1=";
                X1.Visibility = Visibility.Visible;
                X1.Text = x1.ToString();
            }
            else
            {
                MessageBox.Show("Уравнение не имеет решения");
            }
        }
    }
}
