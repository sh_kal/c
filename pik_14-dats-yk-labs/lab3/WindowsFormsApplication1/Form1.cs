﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int a, b, c, d;
            double x1, x2;
            a = int.Parse(textBox1.Text);
            b = int.Parse(textBox2.Text);
            c = int.Parse(textBox3.Text);
            d = b * b - 4 * a * c;
            label4.Visible = true;
            label4.Text = "D = ";
            textBox4.Visible = true;
            textBox4.Text = d.ToString();
            if (d > 0)
            {
                x1 = (-b + Math.Sqrt(d)) / (2 * a);
                x2 = (-b - Math.Sqrt(d)) / (2 * a);
                label5.Visible = true;
                label5.Text = "x1 = ";
                textBox5.Visible = true;
                textBox5.Text = x1.ToString();
                label6.Visible = true;
                label6.Text = "x2 = ";
                textBox6.Visible = true;
                textBox6.Text = x2.ToString();
            }
            else if (d == 0)
            {
                x1 = -b / (2 * a);
                label5.Visible = true;
                label5.Text = "x = ";
                textBox5.Visible = true;
                textBox5.Text = x1.ToString();
            }
            else
            {
                MessageBox.Show("Уравнение не имеет решения");
            }
        }
    }
}
