﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhileConsole22
{
    class Program
    {
        static void Main(string[] args)
        {
            int k = 0;
            double P = 0, sum = 10, sum1=0;
            Console.Write("Введите количество процентов от пробега предыдущего дня(0<P<50) ");
            P = double.Parse(Console.ReadLine());
            if (P > 0 && P < 50)
            {
                do
                {
                    sum1 = sum * P / 100;
                    sum += sum1;
                    k++;
                }
                while (sum <= 200);
                Console.WriteLine("Суммарный пробег {0:0.###} за {1} дней", sum, k);
            }
            else {
                Console.WriteLine("Неправильный ввод");
            }
        }
    }
}
