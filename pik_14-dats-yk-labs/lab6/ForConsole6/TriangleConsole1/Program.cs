﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriangleConsole1
{
    class Program
    {
        static void pointIn(int[] x,int[] y)
        {
            Console.WriteLine("Введике координаты вершины А: ");
            x[1] = int.Parse(Console.ReadLine());
            y[1] = int.Parse(Console.ReadLine());
            Console.WriteLine("Введике координаты вершины В: ");
            x[2] = int.Parse(Console.ReadLine());
            y[2] = int.Parse(Console.ReadLine());
            Console.WriteLine("Введике координаты вершины С: ");
            x[3] = int.Parse(Console.ReadLine());
            y[3] = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите координаты точки:");
            x[0] = int.Parse(Console.ReadLine());
            y[0] = int.Parse(Console.ReadLine());
        }
        static void pointInTriangle(int[] x,int[] y)
        {
            int point1, point2, point3;
            point1 = (x[1] - x[0]) * (y[2] - y[1]) - (x[2] - x[1]) * (y[1] - y[0]);
            point2 = (x[2] - x[0]) * (y[3] - y[2]) - (x[3] - x[2]) * (y[2] - y[0]);
            point3 = (x[3] - x[0]) * (y[1] - y[3]) - (x[1] - x[3]) * (y[3] - y[0]);
            if ((point1 <= 0 && point2 <= 0 && point3 <= 0) || (point1 >= 0 && point2 >= 0 && point3 >= 0))
                Console.WriteLine("Точка пренадлежит треугольнику");
            else
                Console.WriteLine("Точка не пренадлежит треугольнику");
        }
        static void Main(string[] args)
        {
            int[] pointX = new int[4];
            int[] pointY = new int[4];
            pointIn(pointX, pointY);
            pointInTriangle(pointX, pointY);
        }
    }
}
