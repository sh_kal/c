﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab6
{
    class Program
    {
        static void Main(string[] args)
        {
            double n, i, sum=0;
            Console.Write("Введите n: ");
            n = double.Parse(Console.ReadLine());
            for (i = 0; i < n; i++)
            {
                sum += Math.Pow(i, n - i);
            }
            Console.WriteLine("Сумма = {0}",sum);
        }
    }
}
