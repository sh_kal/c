﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PiConsole
{
    class Program
    {
        static double frViet(int n)
        {
            int i;
            double vr = 0.5 + Math.Sqrt(0.5) / 2;
            double vp = Math.Sqrt(0.5) * Math.Sqrt(vr);
            for (i = 0; i <= n; i++)
            {
                vr = 0.5 + Math.Sqrt(vr) / 2;
                vp *= Math.Sqrt(0.5 + (Math.Sqrt(vr) / 2));
            }
            return 2 / vp;
        }
        static double jWall(int n)
        {
            double dr = 1;
            for (double i = 0, z = 1, x = 2; i <= n; i++, z += 2, x += 2)
                dr *= (z * (z + 2)) / (x * x);
            return 2 / dr;
        }
        static double lBrunck(int n)
        {
            double lBr = 1, lBz = 1;
            for (int i = 0; i <= n; i++, lBz++)
                lBr = 1 + 1 / (2 + lBz / (lBz + 2) * (lBz + 2));
            return 4 / lBr;
        }
        static double Leyb(int n)
        {
            double lr = 0, lz = 1;
            for (int i = 0; i <= n; i++, lz += 4)
                lr += 1 / lz - 1 / (lz + 2);
            return lr * 4;
        }
        static void Main(string[] args)
        {
            int i;
            Console.WriteLine("Введите количество итераций: ");
            i = int.Parse(Console.ReadLine());
            Console.WriteLine("Франсуа Виет: {0}\tДжон Уоллис {1}\tЛорд Брункер {2}\tЛейбниц {3}",frViet(i),jWall(i),lBrunck(i),Leyb(i));
        }
    }
}
