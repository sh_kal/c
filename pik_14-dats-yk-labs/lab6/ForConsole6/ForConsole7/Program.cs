﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForConsole7
{
    class Program
    {
        static void Main(string[] args)
        {
            double n, i, k, sum = 0;
            Console.Write("Insert n ");
            n = double.Parse(Console.ReadLine());
            Console.Write("Insert k ");
            k = double.Parse(Console.ReadLine());
            if (n > 0)
            {
                for (i = 0; i < n; i++)
                {
                    sum += Math.Pow(i, k);
                }
                Console.WriteLine("Result = {0}", sum);
            }
            else
            {
                Console.WriteLine("Error");
            }
        }
    }
}
