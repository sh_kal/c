﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForConsole8
{
    class Program
    {
        static void Main(string[] args)
        {
            double i=0, n;
            Console.Write("Insert n ");
            n = double.Parse(Console.ReadLine());
            if (n > 1)
            {
                while (Math.Pow(3,i)<n)
                {
                    i++;
                }
                Console.WriteLine("k = {0}", i);
            }
            else
            {
                Console.WriteLine("Error");
            }
        }
    }
}
