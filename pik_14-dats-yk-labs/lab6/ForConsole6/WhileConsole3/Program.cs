﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhileConsole3
{
    class Program
    {
        static void Main(string[] args)
        {
            int a=1, sum = 0;
            Console.WriteLine("Введите последовательность чисел, завершая введение 0");
            while (a != 0)
            {
                a = int.Parse(Console.ReadLine());
                sum += a;
            }
            Console.WriteLine("Сумма {0}", sum);
        }
    }
}
