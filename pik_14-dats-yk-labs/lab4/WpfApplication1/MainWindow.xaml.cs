﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApplication1
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            //textBox.ToolTip = "Введите число в диапазоне от 1 до 7";
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            label.Visibility = Visibility.Visible;
            int q;
            q = int.Parse(textBox.Text);
            switch (q)
            {
                case 1:
                    label.Content = "Понедельник";
                    break;
                case 2:
                    label.Content = "Вторник";
                    break;
                case 3:
                    label.Content = "Среда";
                    break;
                case 4:
                    label.Content = "Четверг";
                    break;
                case 5:
                    label.Content = "Пятница";
                    break;
                case 6:
                    label.Content = "Суббота";
                    break;
                case 7:
                    label.Content = "Воскресенье";
                    break;
                default:
                    label.Visibility = Visibility.Hidden;
                    MessageBox.Show("Неправильная команда");
                    break;
            }
            textBox.Text = " ";
            //textBox.ToolTip = "Введите число в диапазоне от 1 до ";
        }

        private void mounth_Click(object sender, RoutedEventArgs e)
        {
            label.Visibility = Visibility.Visible;
            int q;
            q = int.Parse(textBox.Text);
            switch (q)
            {
                case 1:
                    label.Content = "Январь";
                    break;
                case 2:
                    label.Content = "Февраль";
                    break;
                case 3:
                    label.Content = "Март";
                    break;
                case 4:
                    label.Content = "Апрель";
                    break;
                case 5:
                    label.Content = "Май";
                    break;
                case 6:
                    label.Content = "Июнь";
                    break;
                case 7:
                    label.Content = "Июль";
                    break;
                case 8:
                    label.Content = "Август";
                    break;
                case 9:
                    label.Content = "Сентябрь";
                    break;
                case 10:
                    label.Content = "Октябрь";
                    break;
                case 11:
                    label.Content = "Ноябрь";
                    break;
                case 12:
                    label.Content = "Декабрь";
                    break;
                default:
                    label.Visibility = Visibility.Hidden;
                    MessageBox.Show("Неправильная команда");
                    break;
            }
            textBox.Text = " ";
        }

        private void operation_Click(object sender, RoutedEventArgs e)
        {
            label.Visibility = Visibility.Visible;
            string q;
            double a, b;
            q = textBox_Copy1.Text;
            a = double.Parse(textBox.Text);
            b = double.Parse(textBox_Copy.Text);
            switch (q)
            {
                case "+":
                    a = a + b;
                    label.Content = a;
                    break;
                case "-":
                    a = a - b;
                    label.Content = a;
                    break;
                case "*":
                    a = a * b;
                    label.Content = a;
                    break;
                case "/":
                    a = a / b;
                    label.Content = a;
                    break;
                default:
                    label.Visibility = Visibility.Hidden;
                    MessageBox.Show("Неправильная команда");
                    break;
            }
        }

        private void exit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
