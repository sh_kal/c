﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4
{
    class Program
    {
        static void Day()
        {
            int q;
            Console.Write("Введите число в диапазоне от 1 до 7: ");
            q = int.Parse(Console.ReadLine());
            switch (q)
            {
                case 1:
                    Console.WriteLine("Понедельник");
                    break;
                case 2:
                    Console.WriteLine("Вторник");
                    break;
                case 3:
                    Console.WriteLine("Среда");
                    break;
                case 4:
                    Console.WriteLine("Четверг");
                    break;
                case 5:
                    Console.WriteLine("Пятница");
                    break;
                case 6:
                    Console.WriteLine("Суббота");
                    break;
                case 7:
                    Console.WriteLine("Воскресенье");
                    break;
                default:
                    Console.WriteLine("Неверная команда");
                    break;
            }
        }
        static void Month()
        {
            int q;
            Console.Write("Введите число в диапазоне от 1 до 12: ");
            q = int.Parse(Console.ReadLine());
            switch (q)
            {
                case 1:
                    Console.WriteLine("Январь");
                    break;
                case 2:
                    Console.WriteLine("Фефраль");
                    break;
                case 3:
                    Console.WriteLine("Март");
                    break;
                case 4:
                    Console.WriteLine("Апрель");
                    break;
                case 5:
                    Console.WriteLine("Май");
                    break;
                case 6:
                    Console.WriteLine("Июнь");
                    break;
                case 7:
                    Console.WriteLine("Июль");
                    break;
                case 8:
                    Console.WriteLine("Август");
                    break;
                case 9:
                    Console.WriteLine("Сентябрь");
                    break;
                case 10:
                    Console.WriteLine("Октябрь");
                    break;
                case 11:
                    Console.WriteLine("Ноябрь");
                    break;
                case 12:
                    Console.WriteLine("Декабрь");
                    break;
                default:
                    Console.WriteLine("Неверная команда");
                    break;
            }
        }
        static void operation()
        {
            int q;
            double a, b;
            Console.Write("Введите a: ");
            a = float.Parse(Console.ReadLine());
            Console.Write("Введите b: ");
            b = float.Parse(Console.ReadLine());
            Console.WriteLine("1. Сложение\n2. Умножение\n3. Вычитание\n4. Деление");
            q = int.Parse(Console.ReadLine());
            switch (q)
            {
                case 1:
                    a = a + b;
                    Console.WriteLine("Результат: "+a.ToString());
                    break;
                case 2:
                    a = a * b;
                    Console.WriteLine("Результат: " + a.ToString());
                    break;
                case 3:
                    a = a - b;
                    Console.WriteLine("Результат: " + a.ToString());
                    break;
                case 4:
                    a = a / b;
                    Console.WriteLine("Результат: " + a.ToString());
                    break;
                default:
                    Console.WriteLine("Неверная команда");
                    break;
            }
        }
        static void menu()
        {
            int q;
            bool w = true;
            while (w)
            {
                Console.WriteLine("1. Расшифровка дня недели\n2. Расшифровка месяца\n3. Выполнение операций над числами\n4. Выход");
                q = int.Parse(Console.ReadLine());
                switch (q)
                {
                    case 1:
                        Day();
                        break;
                    case 2:
                        Month();
                        break;
                    case 3:
                        operation();
                        break;
                    case 4:
                        w = false;
                        break;
                    default:
                        Console.WriteLine("Неверная команда");

                        break;
                }
            }
        }
        static void Main(string[] args)
        {
            menu();
        }
    }
}
