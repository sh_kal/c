﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Array1
{
    class Program
    {
        static double sum_vid(double[] arr,int n)
        {
            double sum = 0;
            int i;
            for (i = 0; i < n; i++)
            {
                if (arr[i] < 0)
                {
                    sum += arr[i];
                }
            }
            return sum;
        }
        static int max(double[] arr,int n,double elem)
        {
            int i, k = 0;
            for (i = 0; i < n; i++)
                if (elem > arr[i])
                    k++;
            return k;
        }
        static double max_elem(double[] arr,int n)
        {
            int i;
            double max_ = 0;
            for (i = 0; i < n; i++)
            {
                if (max(arr, n, arr[i]) == (n - 1))
                {
                    max_ = arr[i];
                }
            }
            return max_;
        }
        static int max_a(double[] arr, int n, double elem)
        {
            int i, k = 0;
            for (i = 0; i < n; i++)
                if (Math.Abs(elem) > Math.Abs(arr[i]))
                    k++;
            return k;
        }
        static double max_elem_abs(double[] arr, int n)
        {
            int i;
            double max_ = 0;
            for (i = 0; i < n; i++)
            {
                if (max_a(arr, n, arr[i]) == (n - 1))
                {
                    max_ = arr[i];
                }
            }
            return max_;
        }
        static double sum_numb(double[] arr,int n)
        {
            int i;
            double  sum = 0;
            for (i = 0; i < n; i++)
            {
                if (arr[i] > 0)
                {
                    sum += i;
                }
            }
            return sum;
        }
        static int in_mas(double[] arr,int n)
        {
            int k = 0;
            for (int i = 0; i < n; i++)
            {
                if (arr[i] == Convert.ToInt32(arr[i]))
                {
                    k++;
                }
            }
            return k;
        }
        static void menu1(double[] arr,int n)
        {
            int q, i;
            bool t = true;
            while (t)
            {
                Console.WriteLine("1. Сумма отрицателльных элементов\n2. Максимальный элемент\n3. Номер максимального  элемента\n4. Максимальный по модулю элемент\n5. Сумма индексов положительных элементов\n6. Количество целых чисел в массиве\n7. Выход");
                q = int.Parse(Console.ReadLine());
                switch (q)
                {
                    case 1:
                        Console.Clear();
                        if (sum_vid(arr, n) != 0)
                            Console.WriteLine("Сумма отрицательных элементов {0}", sum_vid(arr, n));
                        else
                            Console.WriteLine("Массив не имеет отрицательных элементов");
                        break;
                    case 2:
                        Console.Clear();
                        Console.WriteLine("Максимальный элемент {0}",max_elem(arr, n));
                        break;
                    case 3:
                        Console.Clear();
                        for (i = 0; i < n; i++)
                        {
                            if (max_elem(arr, n) == arr[i])
                                Console.WriteLine("Номер максимального элемента {0}", i);
                        }
                        break;
                    case 4:
                        Console.Clear();
                        Console.WriteLine("Максимальный по модулю элемент {0}", max_elem_abs(arr, n));
                        break;
                    case 5:
                        Console.Clear();
                        if (sum_numb(arr, n) != 0)
                            Console.WriteLine("Сумма индексов положительных элементов {0}", sum_numb(arr, n));
                        else
                            Console.WriteLine("Массив не имеет положительных элементов элементов");
                        break;
                    case 6:
                        Console.Clear();
                        Console.WriteLine("Количество целых элементов {0}", in_mas(arr, n));
                        break;
                    case 7:
                        t = false;
                        break;
                    default:
                        Console.WriteLine("Неправильный ввод");
                        break;
                }
            }
        }
        static double f_vid_pr(double[] arr,int  n)
        {
            int q=-1;
            double pr = 0.5;
            double pr1 = 1.0;
            for(int i = 0; i < n; i++)
            {
                if (arr[i] < 0)
                {
                    q = i;
                    break;
                }
            }
            if (q != -1)
            {
                for (int i = q+1; i < n; i++)
                {
                    pr1 *= arr[i];
                }
                pr = pr1;
            }
            return pr;
        }
        static double sum_vid_pos(double[] arr,int n)
        {
            int q = -1, w = -1, k = 0;
            double sum = 0;
            for(int i = 0; i < n; i++)
                if (arr[i] < 0)
                {
                    q = i;
                    break;
                }
            for (int i = 0; i < n; i++)
            {
                if (arr[i] > 0)
                    k++;
                if (k == 2)
                {
                    w = i;
                    break;
                }
            }
            if (q < w && q != -1 && w != -1)
            {
                for (int i = q + 1; i < w; i++)
                    sum += arr[i];
                return sum;
            }
            else
                return 0.000001;
        }
        static double sum_null(double[] arr,int n)
        {
            int q = 0,w = 0;
            double sum = 0;
            for (int i = 0; i < n; i++)
                if (arr[i] == 0)
                {
                    q = i;
                    break;
                }
            for(int i = n - 1; i >= 0; i--)
                if (arr[i] == 0)
                {
                    w = i;
                    break;
                }
            if (q != w && q != w - 1)
            {
                for (int i = q; i < w; i++)
                    sum += arr[i];
                return sum;
            }
            else
                return 0.000001;
        }
        static double p_minmax(double[] arr,int n)
        {
            int q = 0, k = 0;
            double min = 0, max = 0, pr = 1;
            for(int i = 0; i < n; i++)
            {
                if (max_a(arr, n, arr[i]) == (n - 1))
                    q = i;
                if (max_a(arr, n, arr[i]) == 0)
                    k = i;
            }
            if (q < k && q != k - 1)
            {
                for (int i = q + 1; i < k; i++)
                    pr *= arr[i];
                return pr;
            }
            else if (k < q && k != q - 1)
            {
                for (int i = k + 1; i < q; i++)
                    pr *= arr[i];
                return pr;
            }
            else
                return 0.000001;
        }
        static void menu2(double[] arr,int n)
        {
            int q, i;
            bool t = true;
            while (t)
            {
                Console.WriteLine("1. Произведение элементов массива, что находятся после отрицательного элемента\n2. Сумма элементов между первым отрицательным и вторым положительным элементами\n3. Сумма элементов между первым и последним нулевыми элементами\n4. Произведение элементов, что находятся между максимальным и мигимальным по модулю элементами \n5. Выход");
                q = int.Parse(Console.ReadLine());
                switch (q)
                {
                    case 1:
                        Console.Clear();
                        if (f_vid_pr(arr, n) != 0.5)
                            Console.WriteLine("Произведение равно {0:0.###}", f_vid_pr(arr, n));
                        else
                            Console.WriteLine("В массиве нет отрицательных элементов");
                            break;
                    case 2:
                        Console.Clear();
                        if (sum_vid_pos(arr, n) != 0.000001)
                            Console.WriteLine("Сумма {0:0.###}", sum_vid_pos(arr, n));
                        else
                            Console.WriteLine("Невозможно посчитать сумму");
                        break;
                    case 3:
                        Console.Clear();
                        if (sum_null(arr, n) != 0.000001)
                            Console.WriteLine("Сумма {0:0.###}", sum_null(arr, n));
                        else
                            Console.WriteLine("Невозможно посчитать сумму");
                        break;
                    case 4:
                        Console.Clear();
                        if (p_minmax(arr,n) != 0.000001)
                            Console.WriteLine("Произведение {0:0.###}", p_minmax(arr, n));
                        else
                            Console.WriteLine("Невозможно посчитать произведение");
                        break;
                    case 5:
                        t = false;
                        break;
                    default:
                        Console.WriteLine("Неправильный ввод");
                        break;
                }
            }
        }
        static void menu(double[] arr,int n)
        {
            int q;
            bool t = true;
            while (t)
            {
                Console.WriteLine("1. Задание 1 \n2. Задание 2 \n3. Выход");
                q = int.Parse(Console.ReadLine());
                switch (q)
                {
                    case 1:
                        Console.Clear();
                        menu1(arr, n);
                        break;
                    case 2:
                        Console.Clear();
                        menu2(arr, n);
                        break;
                    case 3:
                        t = false;
                        break;
                    default:
                        Console.WriteLine("Неправильный ввод");
                        break;
                }
            }
        }
        static void Main(string[] args)
        {
            int n, i;
            Console.Write("Введите количество элементов массива ");
            n = int.Parse(Console.ReadLine());
            double[] arr = new double[n];
            Console.WriteLine("Введите элементы массива ");
            for (i = 0; i < n; i++)
            {
                Console.Write("arr[" + i + "] = ");
                arr[i] = double.Parse(Console.ReadLine());
            }
            menu(arr, n);
        }
    }
}
