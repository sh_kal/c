﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrix
{
    class Matrix
    {
        private static int n, m;
        private int[,] a;
        public Matrix(int _n,int _m)
        {
            n = _n;
            m = _m;
            a = new int[n, m];
        }
        public Matrix(int _n, int _m, int l)
        {
            n = _n;
            m = _m;
            a = new int[n, m];
            for (int i = 0; i < n; i++)
                for (int j = 0; j < m; j++)
                    a[i, j] = l;
        }
        public void MatrixIn()
        {
            Random rand = new Random();
            for (int i = 0; i < a.GetLength(0); i++)
                for (int j = 0; j < a.GetLength(1); j++)
                   a[i, j] = rand.Next(10);
        }
        public void MatrixOut()
        {
            for (int i = 0; i < a.GetLength(0); i++)
            {
                for (int j = 0; j < a.GetLength(1); j++)
                    Console.Write("{0}\t", a[i, j]);
                Console.WriteLine("\n");
            }
        }
        public static void Proizv(Matrix A,Matrix B)
        {
            int i, j;
            Matrix C = new Matrix(A.a.GetLength(0), B.a.GetLength(1), 0);
            for (i = 0; i < A.a.GetLength(0); i++)
                for (j = 0; j < B.a.GetLength(1); j++)
                    for (int q = 0; q < B.a.GetLength(0); q++)
                        C.a[i, j] += A.a[i, q] * B.a[q, j];
            Console.WriteLine("Произведение матриц: \n");
            C.MatrixOut();
        }
        public static void GetMAtrix(int [,] arr, int[,] p,int i,int j)
        {
            int li, lj, qi, qj;
            qi = 0;
            for (li = 0; li < arr.GetLength(0) - 1; li++)
            {
                if (li == i)
                    qi = 1;
                qj = 0;
                for (lj = 0; lj < arr.GetLength(0) - 1; lj++)
                {
                    if (lj == j)
                        qj = 1;
                    p[li, lj] = arr[li + qi, lj + qj];
                }
            }
        }
        public static int Determinant(Matrix A,int m)
        {
            int i, j, d, k, n;
            int[,] p=new int[m,m];
            k = 1;
            j = 0;
            d = 0;
            n = m - 1;
            if (m < 1)
                Console.WriteLine("Невозможно посчитать детерминант");
            else if (m == 1)
            {
                d = A.a[0,0];
                return d;
            }
            else if (m == 2)
            {
                d = A.a[0, 0] * A.a[1, 1] - (A.a[1, 0] * A.a[0, 1]);
                return d;
            }
            else
            {
                for (i = 0; i < m; i++)
                {
                    GetMAtrix(A.a, p, i, 0);
                    d = d + k * A.a[i, 0] * Determinant(A, n);
                    k = -k;
                }
            }
            return d;
        }
        public static void Transp(Matrix A)
        {
            Matrix B = new Matrix(A.a.GetLength(1), A.a.GetLength(0));
            int i,j;
            for (i = 0; i < B.a.GetLength(0); i++)
            {
                for (j = 0; j < B.a.GetLength(1); j++)
                {
                    B.a[i, j] = A.a[j, i];
                }
            }
            B.MatrixOut();
        }
    }
}
