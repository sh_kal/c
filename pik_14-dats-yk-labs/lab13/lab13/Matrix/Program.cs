﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Media;

namespace Matrix
{
    class Program
    {
        static void menu(Matrix A,Matrix B, int c, int r)
        {
            bool t = true;
            while (t)
            {
                int q;
                Console.WriteLine("1. Произведение матриц\n2. Детерминант матрицы\n3. Транспонированная матрица\n4. Выход");
                q = int.Parse(Console.ReadLine());
                switch (q)
                {
                    case 1:
                        Console.Clear();
                        Matrix.Proizv(A, B);
                        break;
                    case 2:
                        Console.Clear();
                        if (c == r)
                            Console.WriteLine("Детерминант матрицы: {0}", Matrix.Determinant(A, c));
                        else
                            Console.WriteLine("Невозможно посчитать детерминант");
                        break;
                    case 3:
                        Console.Clear();
                        A.MatrixOut();
                        Console.WriteLine("Транспонированная матрица");
                        Matrix.Transp(A);
                        break;
                    case 4:
                        t = false;
                        break;
                    default:
                        Console.WriteLine("");
                        break;
                }
            }
        }
        static void Main(string[] args)
        {
            int cCols, cRows;
            Console.WriteLine("Введите размер матрицы:");
            cRows = int.Parse(Console.ReadLine());
            cCols = int.Parse(Console.ReadLine());
            Matrix A = new Matrix(cRows, cCols);
            A.MatrixIn();
            Console.Clear();
            Console.WriteLine("Матрица А\n");
            A.MatrixOut();
            Matrix B = new Matrix(cCols, cRows);
            B.MatrixIn();
            Console.WriteLine("Матрица B\n");
            B.MatrixOut();
            menu(A, B, cCols, cRows);
        }
    }
}
