﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForCOnsole5
{
    class Program
    {
        static int fact(int n)
        {
            if (n < 0)
                return 0;
            else if (n == 0)
                return 1;
            else
                return n * fact(n-1);
        }
        static void Main(string[] args)
        {
            int n;
            Console.Write("Введите n: ");
            n = int.Parse(Console.ReadLine());
            Console.WriteLine("{0}",fact(n));
        }
    }
}
