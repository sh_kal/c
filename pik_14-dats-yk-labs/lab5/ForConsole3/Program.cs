﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForConsole3
{
    class Program
    {
        static double func(double a,int n)
        {
            int i = 0;
            double q=1;
            for (i = 0; i < n; i++)
                q *= a;
            return q;
        }
        static void Main(string[] args)
        {
            int  n = 0;
            double a = 0;
            Console.Write("Введите a: ");
            a = double.Parse(Console.ReadLine());
            Console.Write("Введите n: ");
            n = int.Parse(Console.ReadLine());
            a = func(a, n);
            Console.WriteLine("{0}", a);
        }
    }
}
