﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForConsole2
{
    class Program
    {
        static void Main(string[] args)
        {
            int n, i = 0;
            double sum = 0;
            Console.Write("Введите n: ");
            n = int.Parse(Console.ReadLine());
            if (n > 0)
            {
                for (i = 1; i <= n; i++)
                {
                    sum += 1.0 / i;
                }
                Console.WriteLine("{0:0.###}",sum);
            }
            else
            {
                Console.WriteLine("Неккоректный ввод");
            }
        }
    }
}
