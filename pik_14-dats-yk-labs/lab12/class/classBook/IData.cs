﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace classBook
{
    interface IData
    {
        void insert();
        int inspect(string str,Regex reg);
    }
}
