﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace classBook
{
    class Phone:IData
    {
        private string name, firstname, surname, adress, timeIn, timeOut, number;
        public string fName
        {
            set
            {
                Regex reg = new Regex(@"\d");
                if (inspect(value, reg) == 0)
                {
                    Console.WriteLine("Имя не должно содержать цифр");
                    fName = Console.ReadLine();
                }
                else
                    firstname = value;
            }
            get
            {
                return firstname;
            }
        }
        public string Name
        {
            set
            {
                Regex reg = new Regex(@"\d");
                if (inspect(value, reg) == 0)
                {
                    Console.WriteLine("Отчество не должно содержать цифр");
                    Name = Console.ReadLine();
                }
                else
                    name = value;
            }
            get
            {
                return name;
            }
        }
        public string sName
        {
            set
            {
                Regex reg = new Regex(@"\d");
                if (inspect(value, reg) == 0)
                {
                    Console.WriteLine("Фамилия не должна содержать цифр");
                    sName = Console.ReadLine();
                }
                else
                    surname = value;
            }
            get
            {
                return surname;
            }
        }
        public string PhoneNumb
        {
            set
            {
                Regex reg = new Regex(@"\b(\d{3}[ -.]\d{3}[ -.]\d{4})\b");
                if (inspect(value, reg) == 1)
                {
                    Console.Write("Неправильный ввод номера\nВведите номер(ххх ххх хххх): ");
                    PhoneNumb = Console.ReadLine();
                }
                else
                    number = value;
            }
            get
            {
                return number;
            }
        }
        public string Adress
        {
            set
            {
                Regex reg = new Regex(@"г[. ]\w([а-я]{1,}) ул[. ]\w([а-я]{1,}) \d");
                if (inspect(value, reg) == 1)
                {
                    Console.Write("Неправильный ввод, введите адрес еще раз: ");
                    Adress = Console.ReadLine();
                }
                else
                    adress = value;
            }
            get
            {
                return adress;
            }
        }
        public string TimeI
        {
            set
            {
                Regex reg = new Regex(@"\b([0-9]{2,}:[0-5]{1}[0-9]{1})\b");
                if (inspect(value, reg) == 1)
                {
                    Console.Write("Неправильный ввод, введите еще раз: ");
                    TimeI = Console.ReadLine();
                }
                else
                    timeIn = value;
            }
            get
            {
                return timeIn;
            }
        }
        public string TimeO
        {
            set
            {
                Regex reg = new Regex(@"\b([0-9]{2,}:[0-5]{1}[0-9]{1})\b");
                if (inspect(value, reg) == 1)
                {
                    Console.Write("Неправильный ввод, введите еще раз:");
                    TimeO = Console.ReadLine();
                }
                else
                    timeOut = value;
            }
            get
            {
                return timeOut;
            }
        }
        public void insert()
        {
            Console.Write("Введите фамилию: ");
            sName = Console.ReadLine();
            Console.Write("Введите имя: ");
            fName = Console.ReadLine();
            Console.Write("Введите отчество: ");
            Name = Console.ReadLine();
            Console.Write("Введите адрес(например: г.Житомир ул.Черняховского 106): ");
            Adress = Console.ReadLine();
            Console.Write("Введите номер(ххх ххх хххх): ");
            PhoneNumb = Console.ReadLine();
            Console.Write("Введите время городских разговоров(например: 24:78): ");
            TimeI = Console.ReadLine();
            Console.Write("Введите время междугородных разговоров(например: 24:78): ");
            TimeO = Console.ReadLine();
        }
        public void print()
        {
            Console.WriteLine("ФИО {2} {1} {0}\nАдрес проживания {3}\nНомер телефона +38 {4}\nВремя городских разговоров {5}\nВремя междугородних разговоров {6}\n------------------------------------------------------",fName,Name,sName,Adress,PhoneNumb,TimeI,TimeO);
        }
        public int inspect(string str,Regex reg)
        {
            MatchCollection matches = reg.Matches(str);
            if (matches.Count > 0)
                return 0;
            else
                return 1;
        }
    }
}
