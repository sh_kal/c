﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace classBook
{
    class Bus
    {
        private static string FIO, type;
        private static int numberBus, numberWay, cilometres, year;
        public Bus(string name,string _type,int nBus,int nWay,int _year,int cil)
        {
            FIO = name;
            type = _type;
            numberBus = nBus;
            numberWay = nWay;
            year = _year;
            cilometres = cil;
        }

        public void pr()
        {
            Console.WriteLine("Автобус №{2} марки {1} едет по маршруту {3}. Год начала эксплуатации {4}, пробег {3}. Водитель {0}",FIO,type,numberBus,numberWay,year,cilometres);
        }
    }
}
