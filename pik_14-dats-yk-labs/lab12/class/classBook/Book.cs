﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace classBook
{
    class Book:IData
    {
        private int year, count;
        private string Author, BookName, Print;
        public Book()
        {
            year = 2012;
            count = 125;
            Author = "Joan Kate Rouling";
            BookName = "Garry";
            Print = "Hello";
        }
        public Book(int _year, int _count, string _Author, string _Bookname, string _Print)
        {
            year = _year;
            count = _count;
            BookName = _Bookname;
            Author = _Author;
            Print = _Print;
        }
        public Book(Book previousBook)
        {
            year = previousBook.year;
            count = previousBook.count;
            BookName = previousBook.BookName;
            Author = previousBook.Author;
            Print = previousBook.Print;
        }
        public string AuthorName
        {
            set
            {
                Regex reg = new Regex(@"[0-9]");
                if (inspect(value, reg) == 0)
                {
                    Console.Write("Некоректный ввод, ФИО не должно содержать цифры\nВведите автора книги: ");
                    AuthorName = Console.ReadLine();
                }
                else
                    Author = value;
            }
            get
            {
                return Author;
            }
        }
        public string BName
        {
            set
            {
                BookName = value;
            }
            get
            {
                return BookName;
            }
        }

        public string PrintName
        {
            set
            {
                Print = value;
            }
            get
            {
                return Print;
            }
        }

        public int Year
        {
            set
            {
                Regex reg = new Regex(@"\b([1-2]{1}\d{3})\b");
                if (inspect(value.ToString(), reg) == 1)
                {
                    Console.WriteLine("Неправильный ввод даты");
                    Year = int.Parse(Console.ReadLine());
                }
                else if (value > 2016)
                {
                    Console.Write("Год издания не должен превышать текущий год\nВведите год издания: ");
                    Year = int.Parse(Console.ReadLine());
                }
                else
                    year = value;
            }
            get
            {
                return year;
            }
        }

        public int Pages
        {
            set
            {
                count = value;
            }
            get
            {
                return count;
            }
        }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
        public void print()
        {
            Console.WriteLine("{0} написал(a) книгу \"{1}\" на {2} страниц. Издательство {3} выпустило ее в {4} году", AuthorName, BName, Pages, PrintName, Year);
        }
        public void insert()
        {
            Console.Write("Введите автора: ");
            AuthorName = Console.ReadLine();
            Console.Write("Введите название книги: ");
            BName = Console.ReadLine();
            Console.Write("Введите название издательства: ");
            PrintName = Console.ReadLine();
            try
            {
                Console.Write("Введите количество страниц: ");
                Pages = int.Parse(Console.ReadLine());
            }
            catch(FormatException)
            {
                Console.WriteLine("Поле не должно содержать буквы, введите значение еще раз: ");
                Pages = int.Parse(Console.ReadLine());
            }
            try
            {
                Console.Write("Введите год издания: ");
                Year = int.Parse(Console.ReadLine());
            }
            catch (FormatException)
            {
                Console.WriteLine("Поле не должно содержать буквы, введите значение еще раз: ");
                Pages = int.Parse(Console.ReadLine());
            }
        }
        public int inspect(string str,Regex reg)
        {
            MatchCollection matches = reg.Matches(str);
            if (matches.Count > 0)
                return 0;
            else
                return 1;
        }
    }
}
    