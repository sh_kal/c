﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace classBook
{
    class Program
    {
        static void menu(Book[] arr,int counter,Bus a)
        {
            bool t = true;
            while (t)
            {
                int q;
                Console.Write("1. Класс Book\n2. Класс Bus\n3. Вернуться к предыдущему меню\n");
                q = int.Parse(Console.ReadLine());
                switch (q)
                {
                    case 1:
                        Console.Clear();
                        for (int i = 0; i < counter; i++)
                            arr[i].print();
                        break;
                    case 2:
                        Console.Clear();
                        a.pr() ;
                        break;
                    case 4:
                        t = false;
                        break;
                    default:
                        Console.WriteLine("");
                        break;
                }
            }
        }
        static void dynamic()
        {
            bool t = true;
            while (t)
            {
                int q;
                Console.Write("1. Класс Book\n2. Класс Phone\n3. Вернуться к предыдущему меню\n");
                q = int.Parse(Console.ReadLine());
                switch (q)
                {
                    case 1:
                        int counter;
                        Console.Write("Введите к-во элементов массива ");
                        counter = int.Parse(Console.ReadLine());
                        Book[] book = new Book[counter];
                        for (int i = 0; i < counter; i++)
                            book[i] = new Book();
                        for (int i = 0; i < counter; i++)
                            book[i].insert();
                        Console.Clear();
                        for (int i = 0; i < counter; i++)
                            book[i].print();
                        break;
                    case 2:
                        int count;
                        Console.Write("Введите к-во элементов массива ");
                        count = int.Parse(Console.ReadLine());
                        Phone[] phonebook = new Phone[count];
                        for (int i = 0; i < count; i++)
                            phonebook[i] = new Phone();
                        for (int i = 0; i < count; i++)
                            phonebook[i].insert();
                        Console.Clear();
                        for (int i = 0; i < count; i++)
                            phonebook[i].print();
                        break;
                    case 3:
                        t = false;
                        break;
                    default:
                        Console.WriteLine("");
                        break;
                }
            }
        }
        static void Main(string[] args)
        {
            bool t = true;
            while (t)
            {
                int q;
                Console.Write("1. Статические данные\n2. Ввести данные\n3. Выход\n");
                q = int.Parse(Console.ReadLine());
                switch (q)
                {
                    case 1:
                        Console.Clear();
                        Book[] arr = new Book[3];
                        arr[0] = new Book();
                        arr[1] = new Book(2016, 350, "Стивен Кинг", "Противостояние", "Киев");
                        arr[2] = new Book(arr[1]);
                        Bus a = new Bus("Петров П.И.","Мерседес",2,12,2010,20123);
                        menu(arr, 3, a);
                        break;
                    case 2:
                        Console.Clear();
                        dynamic();
                        break;
                    case 3:
                        t = false;
                        break;
                    default:
                        Console.WriteLine("Неверная команда");
                        break;

                }
            }
        }
    }
}