﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static int i, j, p = 0;
        static int per(int[,] arr, int m, int k)
        {
            int a = 0, i;
            for (i = 0; i < m; i++)
            {
                if (arr[k, i] > 0)
                {
                    a++;
                }
            }
            if (a == m)
                return 1;
            else
                return 0;
        }
        static void pr(int[,] arr, int n, int m)
        {
            int d = 0;
            for (i = 0; i < n; i++)
            {
                if (per(arr, m, i) == 1)
                {
                    d = 1;
                    for (j = 0; j < m; j++)
                        d *= arr[i, j];
                }
                p++;
                if (d != 0)
                    Console.WriteLine("Пpоизведение в строке " + p + ": {0}", d);
                d = 0;
            }
        }
        static void print(int[,] arr, int n, int m)
        {
            for (i = 0; i < n; i++)
            {
                for (j = 0; j < m; j++)
                    Console.Write("{0}\t", arr[i, j]);
                Console.Write("\n");
            }
        }
        static int max_dia(int[,] arr, int n)
        {
            int i, j, sum, max = 0, p = 1, max1 = 0;
            sum = 0;
            for (j = 0; j < n - 2; j++)
            {
                for (i = 0; i < n - 1; i++)
                {
                    sum += arr[p,i];
                    p++;
                }
                if (sum > max)
                {
                    max = sum;
                    sum = 0;
                }
            }
            sum = 0;
            p = 1;
            for (j = 0; j < n - 2; j++)
            {
                for (i = 0; i < n - 1; i++)
                {
                    sum += arr[i,p];
                    p++;
                }
                if (sum > max1)
                {
                    max1 = sum;
                    sum = 0;
                }
            }
            if (max > max1)
            {
                return max;
            }
            else
            {
                return max1;
            }
        }
        static int sov_cout(int[,] arr, int n, int m, int k)
        {
            int a = 0, b;
            for (i = 0; i < m - 1; i++) 
            {
                b = i + 1;
                if (arr[k, i] == arr[k, b])
                {
                    a++;
                }
            }
            return a;
        }
        static void sov(int[,] arr,int n,int m)
        {
            int temp = 0;
            for (int r = 0; r < n; r++)
            {
                for (i = 0; i < n; i++)
                {
                    if (sov_cout(arr, n, m, i) > sov_cout(arr, n, m, i + 1))
                    {
                        for (j = 0; j < m; j++)
                        {
                            temp = arr[i, j];
                            arr[i, j] = arr[i + 1, j];
                            arr[i + 1, j] = temp;
                        }
                    }
                }
            }
        }
        static int ret_nv(int[,] arr, int index, int n)
        {
            int k = 0;
            for (i = 0; i < n; i++)
            {
                if (arr[i, index] > 0)
                    k ++;
            }
            if (k == n - 1)
                k = 1;
            else
                k = 0;
            return k;
        }
        static void no_vid(int[,] arr,int n,int m)
        {
            int k = 0;
            for (j = 0; j < m; j++)
            {
                k++;
                if (ret_nv(arr, j, n) != 1)
                {
                    Console.WriteLine("Номер первого столбца без отрицательных элементов {0}", j);
                    break;
                }
            }
            if (k == m)
                Console.WriteLine("Все столбцы имеют отрицательные элементы");
        }
        static void vuvod(int[,] arr, int n, int m)
        {
            for (i = 0; i < n; i++)
            {
                for (j = 0; j < m; j++)
                    Console.Write("{0}\t", arr[i, j]);
                Console.WriteLine("\n");
            }
        }
        static void menu(int[,] arr,int n,int m)
        {
            int q;
            bool t = true;
            while (t)
            {
                Console.WriteLine("1. Произведение в тех строках, где нет нулевых элементов\n2. Максимум среди сум диагоналей, параллельных главной\n3. Сортировка массива\n4. Номер первого столбца, в котором нет орицательных элементов\n5. Выход");
                q = int.Parse(Console.ReadLine());
                switch (q)
                {
                    case 1:
                        Console.Clear();
                        print(arr, n, m);
                        p = 0;
                        pr(arr, n, m);
                        break;
                    case 2:
                        Console.Clear();
                        print(arr, n, m);
                        if (n == m)
                            Console.WriteLine("Максимум: {0}", max_dia(arr, n));
                        else
                            Console.WriteLine("Матрица не квадратня, немозможно найти максимум");
                        break;
                    case 3:
                        Console.Clear();
                        print(arr, n, m);
                        sov(arr, n, m);
                        Console.WriteLine("\n\n");
                        print(arr, n, m);
                        break;
                    case 4:
                        Console.Clear();
                        vuvod(arr, n, m);
                        no_vid(arr, n, m);
                        break;
                    case 5:
                        t = false;
                        break;
                    default:
                        Console.WriteLine("");
                        break;
                }
            }
        }
        static void Main(string[] args)
        {
            int m, n;
            Console.Write("Введите количество строк: ");
            n = int.Parse(Console.ReadLine());
            Console.Write("Введите количество столбцов: ");
            m = int.Parse(Console.ReadLine());
            int[,] arr = new int[n, m];
            Console.WriteLine("Заполните массив: ");
            for (i = 0; i < n; i++)
            {
                for (j = 0; j < m; j++)
                {
                    Console.Write("arr[" + i + "," + j + "]: ");
                    arr[i, j] = int.Parse(Console.ReadLine());
                }
            }
            menu(arr, n, m);
        }
    }
}
