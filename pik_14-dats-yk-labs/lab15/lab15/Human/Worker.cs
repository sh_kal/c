﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Human
{
    class Worker:Human
    {
        private string pos, startWork, endWork;
        private int number;
        public Worker(string name,string g,int y,string p,string sW,string eW,int num) : base(name, g, y)
        {
            pos = p;
            startWork = sW;
            endWork = eW;
            number = num;
        }
        ~Worker(){
            Console.WriteLine("\n Объект был уничтожен");
        }
        public void wOut()
        {
            Console.Write("ФИО: {0}\nПол: {1}\nГод рождения: {2}\nДолжность: {3}\nНачало рабочего дня: {4}\nКонец рабочего дня: {5}\nТабельный номер: {6}",FIO,gender,year,pos,startWork,endWork,number);
        }
    }
}
