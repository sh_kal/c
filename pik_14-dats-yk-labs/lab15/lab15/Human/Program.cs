﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Human
{
    class Program
    {
        static void Main(string[] args)
        {
            int year, number;
            string name, gend, pos, sW, eW;
            Console.WriteLine("Введите ФИО:");
            name = Console.ReadLine();
            Console.WriteLine("Введите пол:");
            gend = Console.ReadLine();
            Console.WriteLine("Введите год рождения:");
            year = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите должность:");
            pos = Console.ReadLine();
            Console.WriteLine("Введите табельный номер");
            number = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите начало рабочего дня:");
            sW = Console.ReadLine();
            Console.WriteLine("Введите конец рабочего дня:");
            eW = Console.ReadLine();
            Worker obj1 = new Worker(name, gend, year, pos, sW, eW, number);
            Console.Clear();
            obj1.wOut();
        }
    }
}
